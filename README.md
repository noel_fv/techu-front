# Polymer App Toolbox - Starter Kit

[![Build Status](https://travis-ci.org/Polymer/polymer-starter-kit.svg?branch=master)](https://travis-ci.org/Polymer/polymer-starter-kit)

El proyecto esta basado en la plantilla de polymer starter-kit

### Setup

##### Prerequisites

First, install [Polymer CLI](https://github.com/Polymer/polymer-cli) using
[npm](https://www.npmjs.com) (we assume you have pre-installed [node.js](https://nodejs.org)).

    npm install -g polymer-cli

Second, install [Bower](https://bower.io/) using [npm](https://www.npmjs.com)

    npm install -g bower

##### Initialize project from template

    mkdir my-app
    cd my-app
    polymer init polymer-2-starter-kit

### Start the development server

This command serves the app at `http://127.0.0.1:8081` and provides basic URL
routing for the app:

    polymer serve

### Build

El comando `polymer build` construye su aplicación de Polymer para despliegues en produccion, usando las opciones de configuración de build proporcionadas por la línea de comandos o en el archivo` polymer.json` de su proyecto.


```
"builds": [
  {
    "preset": "es5-bundled"
  },
  {
    "preset": "es6-bundled"
  },
  {
    "preset": "es6-unbundled"
  }
]
```


Las compilaciones se enviarán a un subdirectorio bajo el directorio `build /` de la siguiente manera:

```
build/
  es5-bundled/
  es6-bundled/
  es6-unbundled/
```

* `es5-bundled` ES5 para que sea compatible con el navegador que soporte el ecma scritp 5.
* `es6-bundled` ES5 para que sea compatible con el navegador que soporte el ecma scritp 6.

### Preview the build

Este comando sirve a su aplicación. Reemplace `build-folder-name` con el nombre de la carpeta de la compilación que desea servir.

    polymer serve build/build-folder-name/
    "public": "build/es5-bundled/",

### Deploy Firebase

Instalar el sigueinte paquete:

` npm install -g firebase-tools `


Loguearse a firebase

` firebase login `

Inicializar el proyecto en firebase

` firebase init `

Desplegar el proyecto en firebase

` firebase deploy `
